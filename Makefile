.PHONY: all
all: titlepage project 

.PHONY: titlepage
titlepage:
	pdflatex titlepage.tex

.PHONY: project
project:
	lyx --export default project.lyx

.PHONY: clean
clean:
	rm -vf *.auxlock *.dpth *.ps *.dvi *.aux *.toc *.idx *.ind *.ilg *.log *.out *.brf *.blg *.bbl *.lyx~ *.run.xml *.bcf
