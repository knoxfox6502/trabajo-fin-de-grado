# Trabajo Fin de Grado #

## Title ##

**Accionamiento de Servo con Realimentación de Fuerza**


## Author ##

Ángel M. García  
Grado en Ingeniería Electrónica Industrial  
Escuela Politécnica Superior - Universidad de Sevilla  


### Academic supervisors: ###

- Enrique Personal Vázquez, PhD.
- Diego Francisco Larios Marín, PhD.


## Abstract ##

This paper will describe an example about how to drive a servo with feedback 
from a force sensor instead of a position sensor. With this type of control, 
the generated torque will be proportional to the control signal and will not 
depend on the position of the arm or the environment.

The implementation is carried out using a digital PID controller written in 
[MicroPython](http://micropython.org/) running on a 
[STM32F4-DISCOVERY](https://www.st.com/en/evaluation-tools/stm32f4discovery.html
) development board.


## License ##

This work is licensed under the Creative Commons BY-NC-SA 3.0 Unported License. 
To view a copy of this license, visit 
<http://creativecommons.org/licenses/by-nc-sa/3.0/>
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.


## Directory contents ##

```
.
├── software        Embedded firmware & GUI
├── biblio          Lists of references
├── Makefile        Project's makefile
├── project.lyx     Main document in LyX format
├── resources       Document assets
└── titlepage.tex   Project's title-page
```
  
<center><small>
CC BY-NC-SA 3.0  
Copyright (c) 2019 Ángel García  
</small></center>
