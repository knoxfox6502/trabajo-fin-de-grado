# Trabajo Fin de Grado #


## Descripción ##

Software para el proyecto "Accionamiento de Servo con Realimentación de Fuerza".


## Contenido de la carpeta ##

```
.
├── LICENSE
├── main.py                  Software del controlador
├── mpconfigboard.h          Fichero de configuración para la STM32F4-Discovery
├── nau7802.py               Driver para el NAU7802
├── qt-gui-application.py    Interfaz gŕafica para el PC
└── README.md
```
