
### THIS CLASS IS NOT TESTED ###

import pyb
from pyb import const

# NAU7802 I2C Address
class Address:
    _NAU7802_I2C_ADDR = const(0x2A)


# NAU7802 register map
class Register:
    _PU_CTRL = const(0x00)
    _CTRL1 = const(0x01)
    _CTRL2 = const(0x02)
    _OCAL1_B2 = const(0x03)
    _OCAL1_B1 = const(0x04)
    _OCAL1_B0 = const(0x05)
    _GCAL1_B3 = const(0x06)
    _GCAL1_B2 = const(0x07)
    _GCAL1_B1 = const(0x08)
    _GCAL1_B0 = const(0x09)
    _OCAL2_B2 = const(0x0A)
    _OCAL2_B1 = const(0x0B)
    _OCAL2_B0 = const(0x0C)
    _GCAL2_B3 = const(0x0D)
    _GCAL2_B2 = const(0x0E)
    _GCAL2_B1 = const(0x0F)
    _GCAL2_B0 = const(0x10)
    _I2C_CTRL = const(0x11)
    _ADCO_B2 = const(0x12)
    _ADCO_B1 = const(0x13)
    _ADCO_B0 = const(0x14)
    _ADC_REG = const(0x15)
    _OTP_B1 = const(0x15)
    _OTP_B0 = const(0x16)
    _REVISION = const(0x1F)
    _PWR_CTRL = const(0x1C)


# PGA gain select
class GainSelect:
    _PGA_GAIN_128 = const(0b111)
    _PGA_GAIN_64 = const(0b110)
    _PGA_GAIN_32 = const(0b101)
    _PGA_GAIN_16 = const(0b100)
    _PGA_GAIN_8 = const(0b011)
    _PGA_GAIN_4 = const(0b010)
    _PGA_GAIN_2 = const(0b001)
    _PGA_GAIN_1 = const(0b000)


# Conversion rate select
class RateSelect:
    _ADC_CR_320SPS = const(0b111)
    _ADC_CR_80SPS = const(0b011)
    _ADC_CR_40SPS = const(0b010)
    _ADC_CR_20SPS = const(0b001)
    _ADC_CR_10SPS = const(0b000)


# LDO Voltage select
class VoltageSelect:
    _LDO_4V5 = const(0b000)
    _LDO_4V2 = const(0b001)
    _LDO_3V9 = const(0b010)
    _LDO_3V6 = const(0b011)
    _LDO_3V3 = const(0b100)
    _LDO_3V0 = const(0b101)
    _LDO_2V7 = const(0b110)
    _LDO_2V4 = const(0b111)

# NAU7802 load cell driver
class LoadCell:
    def __init__(self, i2c=3):
        self.i2c = pyb.I2C(i2c)
        self.i2c.init(pyb.I2C.MASTER, baudrate=100000, gencall=False, dma=False)

    def reset(self):
        self.i2c.mem_write(0x01, Address._NAU7802_I2C_ADDR, Register._PU_CTRL)
        pyb.delay(1)
        self.i2c.mem_write(0x00, Address._NAU7802_I2C_ADDR, Register._PU_CTRL)
        pyb.delay(1)

    def power_up(self):
        self.i2c.mem_write(0xAE, Address._NAU7802_I2C_ADDR, Register._PU_CTRL)
        pyb.delay(1)

    def set_vldo_gain(
        self, vldo=VoltageSelect._LDO_4V5, pga_gain=GainSelect._PGA_GAIN_1
    ):
        self.i2c.mem_write(
            ((vldo << 3) | pga_gain), Address._NAU7802_I2C_ADDR, Register._CTRL1
        )

    def set_rate(self, crs=RateSelect._ADC_CR_10SPS):
        self.i2c.mem_write((crs << 4), Address._NAU7802_I2C_ADDR, Register._CTRL2)

    def trigger_calibration(self):
        # I2C.mem_read(data, addr, memaddr, *, timeout=5000, addr_size=8)
        tmp = self.i2c.mem_read(1, Address._NAU7802_I2C_ADDR, Register._CTRL2)
        self.i2c.mem_write(
            (tmp[0] | (1 << 2)), Address._NAU7802_I2C_ADDR, Register._CTRL2
        )
        pyb.delay(1)

    def cycle_start(self):
        tmp = self.i2c.mem_read(1, Address._NAU7802_I2C_ADDR, Register._PU_CTRL)
        self.i2c.mem_write(
            (tmp[0] | (1 << 4)), Address._NAU7802_I2C_ADDR, Register._PU_CTRL
        )

    def loadcell_read(self):
        self.xadc_output = self.i2c.mem_read(
            3, Address._NAU7802_I2C_ADDR, Register._ADCO_B2
        )
        xadc_value_signed = self.to_signed(
            (self.xadc_output[0] << 16)
            + (self.xadc_output[1] << 8)
            + self.xadc_output[2],
            24,
        )
        return xadc_value_signed

    def to_signed(self, input_value, num_bits=24):
        mask = 2 ** (num_bits - 1)
        out = -(input_value & mask) + (input_value & ~mask)
        return out

    def convert_range(self, starting_min, starting_max, ending_min, ending_max, value):
        scale = (ending_max - ending_min) / (starting_max - starting_min)
        out = ending_min + ((value - starting_min) * scale)
        return out
